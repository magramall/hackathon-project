<?php

class ControllerMain
{
    protected $view;

    function __destruct()
    {
        $arData = [];
        if ($this->view){
            foreach ($this->view as $name_val=>$value){
                $arData[$name_val] = $value;
            }
        }
        return json_encode($arData);
    }
}