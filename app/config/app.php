<?php
include 'Config.php';

class App
{
    public $app;

    public function initApp()
    {
    }

    public function DBConnect()
    {
        $arConfig = Config::DBConnectMySQL();
        $dsn = "mysql:host=".$arConfig['host'].";dbname=".$arConfig['db'].";charset=".$arConfig['charset'];
        $opt = [
            PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES   => false,
        ];
        $pdo = new PDO($dsn, $arConfig['user'], $arConfig['pass'], $opt);
        return $pdo;
    }
}