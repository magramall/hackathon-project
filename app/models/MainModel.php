<?php
include '../config/app.php';

class MainModel
{
    private $pdo;
    function __construct()
    {
        $this->pdo = app::DBConnect();
    }

    protected function find($columns, $bind)
    {
        $table = 'hackathon.users';
        $str_query = 'SELECT';
        if ($columns){
            $str_columns = implode(',', $columns);
            $str_query .= ' ' . $str_columns . 'FROM' . $table;
        }else{
            $str_query .= ' *' . 'FROM' . $table;
        }

        if ($bind){
            $str_query .= ' WHERE';

            foreach ($bind as $column=>$value){
                $str_query .= ' ' . $column . '=' . $value . 'AND';
            }
            $str_query = substr($str_query, 1, -3);
        }

        return $this->pdo->query($str_query);
    }
}