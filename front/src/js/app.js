import Vue from './vue'
import Axios from 'axios';

const app = new Vue({
    el: '#root',
    data: {
        news: [],
        newspaper: {},
        newspaperLoaded: false,
        currSource: "",
        isSourceSet: false,
        country: 'ru',
        newsLoading: true
    },
    methods: {
        getNewspaper(title, source, content) {
            this.newspaper = {
                title: title,
                source: source,
                content: content
            }

            this.newspaperLoaded = true;
        },

        getNews() {
            this.newsLoading = true;
            const url = 'https://newsapi.org/v2/top-headlines?' +
                'country=' + this.country + '&' +
                'pageSize=100&' +
                'apiKey=86b419223a7b4bb58ab79de33f2bcc9d';
            Axios.get("http://192.168.120.89:81/").then(response => {
                this.news = response.data
                this.newsLoading = false;
            })
        },

        testGet() {
            Axios.get("http://192.168.120.89:81?page=68").then(response => {
                console.log(response)
            }).catch(e => {
                console.log(e)
            })
        },

        getNewsBySource(source) {
            this.newsLoading = true;
            const urlSource = 'https://newsapi.org/v2/top-headlines?' +
                'sources=' + source + '&' +
                'pageSize=100&' +
                'apiKey=86b419223a7b4bb58ab79de33f2bcc9d';
            Axios.get("http://192.168.120.89:81?feed=" + source).then(response => {
                this.news = response.data
                this.newsLoading = false;
            })
        }
    },
    mounted() {
        this.getNews();
       this.testGet();
    }
})